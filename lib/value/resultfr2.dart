import 'dart:math';

import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:tappi/value/end2.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:tappi/value/end.dart';

import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';


class resultfr2 extends StatefulWidget {
  resultfr2({this.sizeofbottle,this.diametro,this.zucchero,this.linear,this.anni});

  
  final String sizeofbottle,diametro,linear;
  final zucchero, anni;
  @override
  _resultfr2State createState() => new _resultfr2State();
}

class _resultfr2State extends State<resultfr2> {
  var zucchero;
  var anni;
  var pc02;
  var pc0212;
  var tao;

  var anniconvert;

  var temp20;

   var snack;

  var vbottle,risultato1,risultato2;

  

 


  var kappa,ossigeno,ossigenov;


  double years;

  double yearsbubble;
  double yearsbubbled;

  int uno, due, unor,duer;

  double negativo;
  String sizeofbottle,diametro,linear;

  double yearsd;

  @override
  void initState() {
    // TODO: implement initState
    
    super.initState();
    getlang().then(updatename);

    zucchero=widget.zucchero;
    sizeofbottle=widget.sizeofbottle;
    diametro=widget.diametro;
    linear=widget.linear;
    anni=widget.anni;

    anniconvert=double.parse(anni);
    //anniconvert=int.parse(anni);

    print(zucchero);
    print(sizeofbottle);
    print(diametro);
    print(linear);
    print(anniconvert);

    if(sizeofbottle=='Standard (75cL)'){
      if(zucchero=='15'){
        pc02=3.42;
      }
      if(zucchero=='16'){
        pc02=3.65;
      }
      if(zucchero=='17'){
        pc02=3.88;
      }
      if(zucchero=='18'){
        pc02=4.10;
      }
      if(zucchero=='19'){
        pc02=4.33;
      }
      if(zucchero=='20'){
        pc02=4.56;
      }
      if(zucchero=='21'){
        pc02=4.79;
      }
      if(zucchero=='22'){
        pc02=5.02;
      }
      if(zucchero=='23'){
        pc02=5.24;
      }
      if(zucchero=='24'){
        pc02=5.47;
      }
      if(zucchero=='25'){
        pc02=5.70;
      }
      if(zucchero=='26'){
        pc02=5.93;
      }

      

    }

    if(sizeofbottle=='Magnum (150cL)'){
      if(zucchero=='15'){
        pc02=3.46;
      }
      if(zucchero=='16'){
        pc02=3.69;
      }
      if(zucchero=='17'){
        pc02=3.92;
      }
      if(zucchero=='18'){
        pc02=4.15;
      }
      if(zucchero=='19'){
        pc02=4.38;
      }
      if(zucchero=='20'){
        pc02=4.61;
      }
      if(zucchero=='21'){
        pc02=4.84;
      }
      if(zucchero=='22'){
        pc02=5.07;
      }
      if(zucchero=='23'){
        pc02=5.30;
      }
      if(zucchero=='24'){
        pc02=5.53;
      }
      if(zucchero=='25'){
        pc02=5.76;
      }
      if(zucchero=='26'){
        pc02=5.99;
      }

    }

     if(sizeofbottle=='Demi (37,5cL)'){
      if(zucchero=='15'){
        pc02=3.40;
      }
      if(zucchero=='16'){
        pc02=3.63;
      }
      if(zucchero=='17'){
        pc02=3.85;
      }
      if(zucchero=='18'){
        pc02=4.08;
      }
      if(zucchero=='19'){
        pc02=4.31;
      }
      if(zucchero=='20'){
        pc02=4.53;
      }
      if(zucchero=='21'){
        pc02=4.76;
      }
      if(zucchero=='22'){
        pc02=4.99;
      }
      if(zucchero=='23'){
        pc02=5.21;
      }
      if(zucchero=='24'){
        pc02=5.44;
      }
      if(zucchero=='25'){
        pc02=5.66;
      }
      if(zucchero=='26'){
        pc02=5.89;
      }

    }

    print('pc02 $pc02'); 
    
    if(sizeofbottle=='Standard (75cL)'){
    
    if(linear=='TOP'){
      if(diametro=='29mm'){
        tao=19.86;
      }
      if(diametro=='26mm'){
        tao=21.94;
      }

    }

    
    if(linear=='TOP+3'){
      if(diametro=='29mm'){
        tao=41.59;
      }
      if(diametro=='26mm'){
        tao=45.94;
      }

    }

    if(linear=='TOP+'){
      if(diametro=='29mm'){
        tao=60.50;
      }
      if(diametro=='26mm'){
        tao=66.82;
      }

    }

     if(linear=='TOP Z'){
      if(diametro=='29mm'){
        tao=73.94;
      }
      if(diametro=='26mm'){
        tao=81.66;
      }

    }
    if(linear=='TOP S'){
      if(diametro=='29mm'){
        tao=102.83;
      }
      if(diametro=='26mm'){
        tao=113.07;
      }

    }


    }

    if(sizeofbottle=='Demi (37,5cL)'){
    
    if(linear=='TOP'){
      if(diametro=='29mm'){
        tao=10.02;
      }
      if(diametro=='26mm'){
        tao=11.07;
      }

    }

    
    if(linear=='TOP+3'){
      if(diametro=='29mm'){
        tao=20.99;
      }
      if(diametro=='26mm'){
        tao=23.18;
      }

    }

    if(linear=='TOP+'){
      if(diametro=='29mm'){
        tao=30.52;
      }
      if(diametro=='26mm'){
        tao=33.71;
      }

    }

     if(linear=='TOP Z'){
      if(diametro=='29mm'){
        tao=37.31;
      }
      if(diametro=='26mm'){
        tao=41.20;
      }

    }
    if(linear=='TOP S'){
      if(diametro=='29mm'){
        tao=51.66;
      }
      if(diametro=='26mm'){
        tao=57.05;
      }

    }


    }

    if(sizeofbottle=='Magnum (150cL)'){
    
    if(linear=='TOP'){
      if(diametro=='29mm'){
        tao=39.11;
      }
     

    }

    
    if(linear=='TOP+3'){
      if(diametro=='29mm'){
        tao=81.89;
      }
     
    }

    if(linear=='TOP+'){
      if(diametro=='29mm'){
        tao=119.11;
      }
      

    }

     if(linear=='TOP Z'){
      if(diametro=='29mm'){
        tao=145.58;
      }
      

    }
    if(linear=='TOP S'){
      if(diametro=='29mm'){
        tao=201.58;
      }
     
    }


    }

     print('Tao $tao');


     
      if(diametro=='29mm'){
      snack=2.4053*pow(10,-4);

      if(linear=='TOP'){
    
    kappa=9.0643*pow(10,-13);
     

      }

    
    if(linear=='TOP+3'){
     
    kappa= 4.3292*pow(10,-13);
    }

    if(linear=='TOP+'){
   
      kappa= 2.9763*pow(10,-13);

    }

     if(linear=='TOP Z'){
     
      kappa= 2.4352*pow(10,-13);

    }
    if(linear=='TOP S'){
     
    kappa=  1.7587*pow(10,-13);
    }

    }




      if(diametro=='26mm'){
      snack=2.0867*pow(10,-4);
        if(linear=='TOP'){
    
    kappa=8.2068*pow(10,-13);
     

      }

    
    if(linear=='TOP+3'){
     
    kappa= 3.9197*pow(10,-13);
    }

    if(linear=='TOP+'){
   
      kappa= 2.6948*pow(10,-13);
   

    }

     if(linear=='TOP Z'){
     
      kappa= 2.2048*pow(10,-13);

    }
    if(linear=='TOP S'){
     
    kappa=  1.5924*pow(10,-13);
    }
      }

      //ho snack

      //trovo kappa


      //vbottle


if(sizeofbottle=='Standard (75cL)'){
vbottle=0.75;
}

if(sizeofbottle=='Demi (37,5cL)'){
vbottle=0.375;
}

if(sizeofbottle=='Magnum (150cL)'){
vbottle=1.5;
}








    














     negativo=(anniconvert/tao)*(-1);
     
     pc0212=pc02*exp(negativo);

     print('salve $pc0212  risultato $negativo');

     temp20=pc0212*1.33;

     

     //years=tao*log(pc0212/2.78);

     //print ("ok anni");

    // print(years); //primo risultato 



   //  yearsd = years.toInt(); 

     //yearsd=yearsd+2020;


  //   yearsbubble=tao*log(pc0212/1.6);  //è il pc012 che cambia
     
 //    print(yearsbubble); //secondo risultato 



    // yearsbubbled=yearsbubble.toInt();

  //   yearsbubbled=yearsbubbled+2020;


     
     yearsd=tao*log(pc0212/2.78)+2020;  //nuovi valori 
     yearsbubbled=tao*log(pc0212/1.6)+2020;  //nuovi valori


     uno =yearsd.toInt();
     due=yearsbubbled.toInt();


     //ora calcolo 02
        var appoggio;
        var appoggio2;
        print("snack");
        print (snack);
        print("kappa");
        print (kappa);

    appoggio=3.0803*pow(10,-2)*snack;
    appoggio2=2.3492*pow(10, 7)*kappa;

     ossigeno=(2.756810*pow(10,11)*snack*kappa)/(appoggio+appoggio2);

     ossigenov=ossigeno/vbottle;

     risultato1=(yearsd-2020)*ossigenov;
      risultato2=(yearsbubbled-2020)*ossigenov;


      unor=risultato1.toInt();
      duer=risultato2.toInt();

     




     

     








    _dropDownMenuItems = getDropDownMenuItems();
    _currentCity = _dropDownMenuItems[0].value;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
  }
String riga1="Nombre d'années jusqu'à la perte de l'AOP VMQ:";
String riga2="Nombre d'années jusqu'à la perte d'effervescence:";
String riga3='En savoir plus';

String testdato='';

Future<String> getlang() async{
     SharedPreferences prefs = await SharedPreferences.getInstance();
     String getdato=prefs.getString("dato");

     return getdato;
}

void updatename(String value){
 print('lingua $value');
 if(value=='Français'){
    print("ciaoo sono francese");
    setState(() {
     this.testdato=value;
     this.riga1="Nombre d'années jusqu'à la perte de l'AOP VMQ:";
     this.riga2="Nombre d'années jusqu'à la perte d'effervescence:";
     this.riga3='En savoir plus';
     
     
    
  });

 }

 if(value=='English'){
    print("ciaoo sono inglese");
    setState(() {
     this.testdato=value;
     this.riga1="Number of years until the loss of the  AOP VMQ";
     this.riga2="Number of years until the loss of effervescence";
     this.riga3='To learn more';
     
     
    
  });

 }
 

  if(value=='Italiano'){
    print("ciaoo sono italiano");
    setState(() {
     this.testdato=value;
     this.riga1="Numero di anni prima della perdita di PDO VMQ ";
     this.riga2="Numero di anni prima della perdita di effervescenza";
     this.riga3='Ulteriori informazioni';
       
  });

 }



  
}

  

    List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

  int _zucchero = 15;
  
  var size;
  int _radioValue = 0;

  int _radioValue2 = 0;



  
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          size = "pz";
          break;
        case 1:
          size = "kg";
          break;
        case 2:
          size = "lt";
          break;
      }
    });
  }

    
  
  List _cities =
  ["1 références", "2 références", "3 références", "4 références",];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCity;

   void changedDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCity = selectedCity;
    });
}

 _launchURL(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
}


  Future<void> send() async {
    final Email email = Email(
      body: "Nombre d'années jusqu'à la perte de l'AOP VMQ : $yearsd \n Nombre d'années jusqu'à la perte d'effervescence: $yearsbubbled ",
  subject: "J'ai calculé les années d'effervescence",
  recipients: ['commercial@pedifrance.com'],
    );

    String platformResponse;

    try {
      print('provo');
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
      print('ok');
    } catch (error) {
      platformResponse = error.toString();
      print('no');
    }

    if (!mounted) return;

    print('bo');
  }






  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomPadding:false,

       floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1),
        elevation: 2.0,
        icon: const Icon(Icons.email),
        label: Text(riga3,textScaleFactor: 1.0,),
        onPressed: (){
          Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => new chiusura2(
                                         valore1: unor,
                                         valore2: duer,
                                        )));
        }
      ),
 
      
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
           
           
          ],
        ),
),

    

     appBar: AppBar(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                    
                    Text("Longévité de ma cuvée",textScaleFactor: 1.0,style: new TextStyle(fontSize:16) ,)
                    
                  ],
                ),
              ),
     body:
      
     
        
    
 

       Column(children: <Widget>[
         

         
         
         
         //qua inizia il blocco inserimento

         
          

        

         
    

             
        Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Date de perte de l'AOP VMQ:",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$uno',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

               Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Quantité d’oxygène absorbée par le vin lors",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                      Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" du vieillissement sur lattes (mg/L) :",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$unor',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

           Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 60,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/lineabolle.png'),
                      ),
                    ),
                  ),
             ),


             Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Date de perte d'effervescence:",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$due',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),


               Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Quantité d’oxygène absorbée par le vin lors",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),

                       Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" du vieillissement sur lattes (mg/L) :",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$duer',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

         
        



          
              Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                  // width: MediaQuery.of(context).size.width,
                   width: 350,
                    height: 75,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/logointro.png'),
                      ),
                    ),
                  ),
             )


      

          




       ],)
     
   
      
     
    
   
    
    );
  }
}

