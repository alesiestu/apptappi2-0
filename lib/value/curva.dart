import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:tappi/value/end.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:tappi/value/end2.dart';



class curva extends StatefulWidget {
  curva({this.years1,this.years2,this.years3,this.oxy});

  
  final years1,years2,years3,oxy;

  @override
  _curvaState createState() => new _curvaState();
}

class _curvaState extends State<curva>{

  var years1;
  var years2;
  var years3;
  var ossigeno;
  var risultato1;
  var risultato2;

   @override
  void initState() {
    years1=widget.years1+2020;
    years2=widget.years2+2020;
    ossigeno=widget.oxy;
    risultato1=(years1-2020)*ossigeno;

    risultato1=risultato1.round();
    
    print(risultato1); 

    risultato2=(years2-2020)*ossigeno;

    risultato2=risultato2.round();

    print(risultato2);
    super.initState();
  }


   

   @override
  Widget build(BuildContext context) {

    
    return Scaffold(
      resizeToAvoidBottomPadding:false,


 
      
 floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1),
        elevation: 2.0,
        icon: const Icon(Icons.email),
        label: Text("En savoir plus",textScaleFactor: 1.0),
        onPressed: (){
        Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => new chiusura2(
                                         valore1: risultato1,
                                         valore2: risultato2,
                                        )));
        }
      ),
      
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
           
           
          ],
        ),
),

    

     appBar: AppBar(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                    Text("Échanges gazeux (O\u2082/CO\u2082) de ma cuvée",textScaleFactor: 1.0,style: new TextStyle(fontSize:14) ,)
                    
                    
                  ],
                ),
              ),
     body:
     
     

       Column(children: <Widget>[
          

         
         
         
         //qua inizia il blocco inserimento

         
          

         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Date de perte de l'AOP VMQ:",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$years1',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

               Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Quantité d’oxygène absorbée par le vin lors",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                      Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" du vieillissement sur lattes (mg/L) :",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$risultato1',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

           Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 60,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/lineabolle.png'),
                      ),
                    ),
                  ),
             ),


             Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Date de perte d'effervescence:",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$years2',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),


               Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" Quantité d’oxygène absorbée par le vin lors",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),

                       Padding(
                       padding: const EdgeInsets.only(top:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           Text(" du vieillissement sur lattes (mg/L) :",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                         ],
                       ),
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text('$risultato2',textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

         
        


        


          


       





      


         
              Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 75,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/logointro.png'),
                      ),
                    ),
                  ),
             )

             
         


    
        


      

          




       ],)
     
   
      
     
    
   
    
    );
             
        


        


      





         
             
         


    
        


      

          




    
     
   
      
     
    
   
    
  
  
  }
}