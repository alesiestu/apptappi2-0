import 'package:flutter/material.dart';

import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter/services.dart';

import 'package:flutter/services.dart';
import 'package:tappi/value/francese.dart';
import 'package:tappi/value/francese.dart' as prefix0;
import 'package:tappi/value/francese2.dart';
import 'package:shared_preferences/shared_preferences.dart';


class chiusura2 extends StatefulWidget {
  chiusura2({this.valore1,this.valore2});
  final valore1, valore2;
  @override
  _chiusura2State createState() => new _chiusura2State();
}

class _chiusura2State extends State<chiusura2> {
int valore1, valore2;
String testdato='';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getlang().then(updatename);

    valore2=widget.valore2;
    valore1=widget.valore1;


  }

  String riga1="Application portant sur l'évaluation des pertes de CO\u2082 modélisées par Gérard LIGER-BELAIR sur la base des valeurs des perméabilités au CO\u2082 des différents joints de capsules couronne de la gamme PE.DI, fournies par le LNE (Laboratoire National d’Essais). Cette simulation est valable dans le cadre d’un sertissage conforme de la capsule couronne et pour un vieillissement en cave à 12°C.";
String riga2='Pour toute information technique et/ou commerciale, contactez notre œnologue à';
String riga3="L'EFFERVESCENCE";

void updatename(String value){
 print('lingua $value');
 if(value=='Français'){
    print("ciaoo sono francese");
    setState(() {
     this.testdato=value;
     this.riga1="Application portant sur l'évaluation des pertes de CO\u2082 modélisées par Gérard LIGER-BELAIR sur la base des valeurs des perméabilités au CO\u2082 des différents joints de capsules couronne de la gamme PE.DI, fournies par le LNE (Laboratoire National d’Essais). Cette simulation est valable dans le cadre d’un sertissage conforme de la capsule couronne et pour un vieillissement en cave à 12°C.";
     this.riga2='Pour toute information technique et/ou commerciale, contactez notre œnologue à';
     this.riga3="L'EFFERVESCENCE";
     
    
  });

 } 

 if(value=='English'){
    print("ciaoo sono inglese");
    setState(() {
     this.testdato=value;
     this.riga1='Application for calculating the CO\u2082 loss through crown cap liners of different CO\u2082 permeabilities.  The project was developed by Gerard Liger-Belair using crown cap liners supplied by PE.DI, and permeability data was supplied by LNE (National Metrology Laboratory of France). This app is valid, assuming a proper crimping of the crown cap and cellar aging at 12˚C.';
     this.riga2='For technical and/or commercial information, contact our expert at:';
     this.riga3="longevity";
     
    
  });

 }

  if(value=='Italiano'){
    print("ciaoo sono italiano");
    setState(() {
     this.testdato=value;
     this.riga1='Applicazione per il calcolo della perdita di CO\u2082 elaborata da Gerard Liger-Belair sulla base delle permeabilità delle diverse guarnizioni dei tappi corona fornite da PE.DI e identificate da LNE (Laboratorio Nazionale di Metrologia - Francia). Questa simulazione è valida solo in caso di una corretta crimpatura del tappo corona e in condizioni di stoccaggio in cantina a 12°C.';
     this.riga2='Per tutte le informazioni tecniche/commerciali contattate il nostro esperto all’indirizzo:  ';
     this.riga3="dell’effervescenza";   
  });

 }


  
  
  
  
}

  Future<String> getlang() async{
     SharedPreferences prefs = await SharedPreferences.getInstance();
     String getdato=prefs.getString("dato");

     return getdato;
  }

  
  Future<void> send() async {
    final Email email = Email(
      body: "Requête d’information envoyée par un utilisateur de l’APP « Effervescence Predictor \n Nombre d'années jusqu'à la perte de l'AOP VMQ : $valore1 \n Nombre d'années jusqu'à la perte d'effervescence: $valore2 ",
  subject: "Requête d’information",
  recipients: ['commercial@pedifrance.com'],
    );

    String platformResponse;

    try {
      print('provo');
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
      print('ok');
    } catch (error) {
      platformResponse = error.toString();
      print('no');
    }

    if (!mounted) return;

    print('bo');
  }
  

    




  @override
  Widget build(BuildContext context) {

    double c_width = MediaQuery.of(context).size.width*0.8;
    
  return Scaffold(
    resizeToAvoidBottomPadding: false,
       appBar: AppBar(
                backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                    Text("Effervescence Predictor",textScaleFactor: 1.0)
                    
                    
                  ],
                ),
        ),
      
      body: Container(
              
              width: double.infinity,
              height: 900,
              decoration: BoxDecoration(
                color: Colors.white
              ),
              child: 
              Padding(
                padding: const EdgeInsets.only(top:60.0),
                child: Column(
                  children: <Widget>[

                           Container (
      padding: const EdgeInsets.all(16.0),
      width: c_width,
      child: new Column (
        children: <Widget>[
          new Text ("$riga1",textScaleFactor: 1.0, style: new TextStyle(fontFamily: 'RobotoSingle',fontSize: 14), textAlign: TextAlign.justify),
        ],
      ),
    ),

                    
             
             Container (
      padding: const EdgeInsets.all(16.0),
      width: c_width,
      child: new Column (
        children: <Widget>[
          new Text ("$riga2",textScaleFactor: 1.0, style: new TextStyle(fontFamily: 'RobotoSingle',fontSize: 14), textAlign: TextAlign.justify),
        ],
      ),
    ),
     
      
      
      Padding(
        padding: const EdgeInsets.only(top:16.0),
        child: new RaisedButton(
                color: Color.fromRGBO(0, 63, 116, 1),
                onPressed: send,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Icon(Icons.share,color: Colors.white,),
                    Text(' '),
                    new Text('commercial@pedifrance.com  ',textScaleFactor: 1.0, style: new TextStyle(fontSize: 16, fontFamily: 'RobotoSingle',color: Colors.white),),
                    
                  ],
                ),
     ),
      ),

             

              



          
          
          
        Expanded(
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: MaterialButton(
              onPressed: () => {},
              child: Padding(
                padding: const EdgeInsets.only(bottom:12.0),
                child: Container(
                      alignment: FractionalOffset.bottomCenter,
                      // width: MediaQuery.of(context).size.width,
                   width: 350,
                    height: 75,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: ExactAssetImage('assets/image/language/logointro.png'),
                        ),
                      ),
                    ),
              ),
            ),
          ),
        ),




                  ],
                ),
              ),
            ),
  );
   
   
      
     
    
   
    
    
  }
}

