import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:tappi/value/end.dart';
import 'package:tappi/value/curva.dart';
import 'package:shared_preferences/shared_preferences.dart';



class resultfr extends StatefulWidget {
  resultfr({this.sizeofbottle,this.diametro,this.zucchero,this.linear,this.anni});

  
  final String sizeofbottle,diametro,linear;
  final zucchero, anni;
  @override
  _resultfrState createState() => new _resultfrState();
}

class _resultfrState extends State<resultfr> {
  var zucchero;
  var anni;
  var pc02;
  var pc0212;
  var tao;

  var years1;

  var years2;

  var ossigeno;
  var resultox;
  var snack;

  var vbottle;

  var anniconvert;

  var temp20;


  var kappa;

  double years;

  double yearsbubble;
  int yearsbubbled;

  var ossigenov;

  double negativo;
  String sizeofbottle,diametro,linear;

  int yearsd;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    getlang().then(updatename);

    zucchero=widget.zucchero;
    sizeofbottle=widget.sizeofbottle;
    diametro=widget.diametro;
    linear=widget.linear;
    anni=widget.anni;

    anniconvert=double.parse(anni);
    //anniconvert=int.parse(anni);

    print(zucchero);
    print(sizeofbottle);
    print(diametro);
    print(linear);
    print(anniconvert);

    if(sizeofbottle=='Standard (75cL)'){
      if(zucchero=='15'){
        pc02=3.42;
      }
      if(zucchero=='16'){
        pc02=3.65;
      }
      if(zucchero=='17'){
        pc02=3.88;
      }
      if(zucchero=='18'){
        pc02=4.10;
      }
      if(zucchero=='19'){
        pc02=4.33;
      }
      if(zucchero=='20'){
        pc02=4.56;
      }
      if(zucchero=='21'){
        pc02=4.79;
      }
      if(zucchero=='22'){
        pc02=5.02;
      }
      if(zucchero=='23'){
        pc02=5.24;
      }
      if(zucchero=='24'){
        pc02=5.47;
      }
      if(zucchero=='25'){
        pc02=5.70;
      }
      if(zucchero=='26'){
        pc02=5.93;
      }

      

    }

    if(sizeofbottle=='Magnum (150cL)'){
      if(zucchero=='15'){
        pc02=3.46;
      }
      if(zucchero=='16'){
        pc02=3.69;
      }
      if(zucchero=='17'){
        pc02=3.92;
      }
      if(zucchero=='18'){
        pc02=4.15;
      }
      if(zucchero=='19'){
        pc02=4.38;
      }
      if(zucchero=='20'){
        pc02=4.61;
      }
      if(zucchero=='21'){
        pc02=4.84;
      }
      if(zucchero=='22'){
        pc02=5.07;
      }
      if(zucchero=='23'){
        pc02=5.30;
      }
      if(zucchero=='24'){
        pc02=5.53;
      }
      if(zucchero=='25'){
        pc02=5.76;
      }
      if(zucchero=='26'){
        pc02=5.99;
      }

    }

     if(sizeofbottle=='Demi (37,5cL)'){
      if(zucchero=='15'){
        pc02=3.40;
      }
      if(zucchero=='16'){
        pc02=3.63;
      }
      if(zucchero=='17'){
        pc02=3.85;
      }
      if(zucchero=='18'){
        pc02=4.08;
      }
      if(zucchero=='19'){
        pc02=4.31;
      }
      if(zucchero=='20'){
        pc02=4.53;
      }
      if(zucchero=='21'){
        pc02=4.76;
      }
      if(zucchero=='22'){
        pc02=4.99;
      }
      if(zucchero=='23'){
        pc02=5.21;
      }
      if(zucchero=='24'){
        pc02=5.44;
      }
      if(zucchero=='25'){
        pc02=5.66;
      }
      if(zucchero=='26'){
        pc02=5.89;
      }

    }

    print('pc02 $pc02'); 
    
    if(sizeofbottle=='Standard (75cL)'){
    
    if(linear=='TOP'){
      if(diametro=='29mm'){
        tao=19.86;
      }
      if(diametro=='26mm'){
        tao=21.94;
      }

    }

    
    if(linear=='TOP+3'){
      if(diametro=='29mm'){
        tao=41.59;
      }
      if(diametro=='26mm'){
        tao=45.94;
      }

    }

    if(linear=='TOP+'){
      if(diametro=='29mm'){
        tao=60.50;
      }
      if(diametro=='26mm'){
        tao=66.82;
      }

    }

     if(linear=='TOP Z'){
      if(diametro=='29mm'){
        tao=73.94;
      }
      if(diametro=='26mm'){
        tao=81.66;
      }

    }
    if(linear=='TOP S'){
      if(diametro=='29mm'){
        tao=102.83;
      }
      if(diametro=='26mm'){
        tao=113.07;
      }

    }


    }

    if(sizeofbottle=='Demi (37,5cL)'){
    
    if(linear=='TOP'){
      if(diametro=='29mm'){
        tao=10.02;
      }
      if(diametro=='26mm'){
        tao=11.07;
      }

    }

    
    if(linear=='TOP+3'){
      if(diametro=='29mm'){
        tao=20.99;
      }
      if(diametro=='26mm'){
        tao=23.18;
      }

    }

    if(linear=='TOP+'){
      if(diametro=='29mm'){
        tao=30.52;
      }
      if(diametro=='26mm'){
        tao=33.71;
      }

    }

     if(linear=='TOP Z'){
      if(diametro=='29mm'){
        tao=37.31;
      }
      if(diametro=='26mm'){
        tao=41.20;
      }

    }
    if(linear=='TOP S'){
      if(diametro=='29mm'){
        tao=51.66;
      }
      if(diametro=='26mm'){
        tao=57.05;
      }

    }


    }

    if(sizeofbottle=='Magnum (150cL)'){
    
    if(linear=='TOP'){
      if(diametro=='29mm'){
        tao=39.11;
      }
     

    }

    
    if(linear=='TOP+3'){
      if(diametro=='29mm'){
        tao=81.89;
      }
     
    }

    if(linear=='TOP+'){
      if(diametro=='29mm'){
        tao=119.11;
      }
      

    }

     if(linear=='TOP Z'){
      if(diametro=='29mm'){
        tao=145.58;
      }
      

    }
    if(linear=='TOP S'){
      if(diametro=='29mm'){
        tao=201.58;
      }
     
    }


    }

     print('Tao $tao');

    
      if(diametro=='29mm'){
      snack=2.4053*pow(10,-4);

      if(linear=='TOP'){
    
    kappa=9.0643*pow(10,-13);
     

      }

    
    if(linear=='TOP+3'){
     
    kappa= 4.3292*pow(10,-13);
    }

    if(linear=='TOP+'){
   
      kappa= 2.9763*pow(10,-13);

    }

     if(linear=='TOP Z'){
     
      kappa= 2.4352*pow(10,-13);

    }
    if(linear=='TOP S'){
     
    kappa=  1.7587*pow(10,-13);
    }

    }




      if(diametro=='26mm'){
      snack=2.0867*pow(10,-4);
        if(linear=='TOP'){
    
    kappa=8.2068*pow(10,-13);
     

      }

    
    if(linear=='TOP+3'){
     
    kappa= 3.9197*pow(10,-13);
    }

    if(linear=='TOP+'){
   
      kappa= 2.6948*pow(10,-13);
   

    }

     if(linear=='TOP Z'){
     
      kappa= 2.2048*pow(10,-13);

    }
    if(linear=='TOP S'){
     
    kappa=  1.5924*pow(10,-13);
    }
      }

      //ho snack

      //trovo kappa


      //vbottle


if(sizeofbottle=='Standard (75cL)'){
vbottle=0.75;
}

if(sizeofbottle=='Demi (37,5cL)'){
vbottle=0.375;
}

if(sizeofbottle=='Magnum (150cL)'){
vbottle=1.5;
}






     negativo=(anniconvert/tao)*(-1);
     
     pc0212=pc02*exp(negativo);  //pco2 initial

     years1=tao*log(pc0212/2.78)+2020;  //nuovi valori 
     years2=tao*log(pc0212/1.6)+2020;  //nuovi valori

     //ora calcolo 02
var appoggio;
var appoggio2;
print("snack");
print (snack);
print("kappa");
print (kappa);

appoggio=3.0803*pow(10,-2)*snack;
appoggio2=2.3492*pow(10, 7)*kappa;

ossigeno=(2.756810*pow(10,11)*snack*kappa)/(appoggio+appoggio2);




print("ossigeno");
print(ossigeno.toString());
     

     
print("ossigeno diviso bottle");

ossigenov=ossigeno/vbottle;

resultox=ossigenov*anniconvert;

//resultox=resultox.round();





print(anniconvert);

print(ossigenov.toString());


     print('salve $pc0212  risultato $negativo');

     temp20=pc0212*1.33;

     

     years=tao*log(pc0212/2.78);

   

     print(years);

     yearsd = years.toInt(); 


     yearsbubble=tao*log(pc0212/1.6);
     print(yearsbubble);
     yearsbubbled=yearsbubble.toInt();





    _dropDownMenuItems = getDropDownMenuItems();
    _currentCity = _dropDownMenuItems[0].value;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
  }

String riga1='Pression de CO\u2082 à 12°C (en bar):';
String riga2='Pression de CO\u2082 à 20°C (en bar):';
String riga3='En savoir plus';

String testdato='';

Future<String> getlang() async{
     SharedPreferences prefs = await SharedPreferences.getInstance();
     String getdato=prefs.getString("dato");

     return getdato;
}

  
void updatename(String value){
 print('lingua $value');
 if(value=='Français'){
    print("ciaoo sono francese");
    setState(() {
     this.testdato=value;
     this.riga1='Pression de CO\u2082 à 12°C (en bar):';
     this.riga2='Pression de CO\u2082 à 20°C (en bar):';
     this.riga3='En savoir plus';
     
     
    
  });

 }

 if(value=='English'){
    print("ciaoo sono inglese");
    setState(() {
     this.testdato=value;
     this.riga1='CO\u2082 pressure at 12°C (bars)';
     this.riga2='CO\u2082 pressure at 20°C (bars)';
     this.riga3='To learn more';
     
     
    
  });

 }
 

  if(value=='Italiano'){
    print("ciaoo sono italiano");
    setState(() {
     this.testdato=value;
     this.riga1='Pressione della CO\u2082 a 12°C (in bar)';
     this.riga2='Pressione della CO\u2082 a 20°C (in bar)';
     this.riga3='Ulteriori informazioni';
       
  });

 }



  
}

  

    List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

  int _zucchero = 15;
  
  var size;
  int _radioValue = 0;

  int _radioValue2 = 0;



  
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          size = "pz";
          break;
        case 1:
          size = "kg";
          break;
        case 2:
          size = "lt";
          break;
      }
    });
  }

    
  
  List _cities =
  ["1 références", "2 références", "3 références", "4 références",];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCity;

   void changedDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCity = selectedCity;
    });
}



 Future<void> send() async {
    final Email email = Email(
      body: "Pression de CO2 à 12°C:  $pc0212 \n Pression de CO2 à 20°C : $temp20",
  subject: "J'ai calculé de pression",
  recipients: ['commercial@pedifrance.com'],
    );

    String platformResponse;

    try {
      print('provo');
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
      print('ok');
    } catch (error) {
      platformResponse = error.toString();
      print('no');
    }

    if (!mounted) return;

    print('bo');
  }





  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomPadding:false,


 
      
 

    

     appBar: AppBar(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                   Text("Échanges gazeux (O\u2082/CO\u2082) de ma cuvée",textScaleFactor: 1.0,style: new TextStyle(fontSize:14) ,)
                    
                    
                  ],
                ),
              ),
     body:
     
     

       Column(children: <Widget>[
          

         
         
         
         //qua inizia il blocco inserimento

         
          

         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Row(
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Text(' $riga1 ',textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                       ],
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text(pc0212.toStringAsFixed(1),textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),

         
         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Row(
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Text(' $riga2 ',textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                       ],
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text(temp20.toStringAsFixed(1),textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),


          Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 60,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/lineabolle.png'),
                      ),
                    ),
                  ),
             ),


          


            Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                   
                     Column(
                       children: [
                         Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           children: <Widget>[
                             Text("Quantité d’oxygène absorbée par le vin lors",textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                           ],
                         ),
                          Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           children: <Widget>[
                             Text('du vieillissement sur lattes (mg/L):',textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                           ],
                         ),
                       ],
                     ),
                   
                     Padding(
                       padding: const EdgeInsets.only(bottom:8.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: <Widget>[

                        

                        Padding(
                          padding: const EdgeInsets.only(top:12.0, right: 20),
                          child: Text(resultox.toStringAsFixed(1),textScaleFactor: 1.0,style: TextStyle(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'), textAlign: TextAlign.left,),
                        )
        


                       ],),
                     ),






                   ],
                 ),
               ),
             ),
           ),
         ),


            Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 60,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/lineabolle.png'),
                      ),
                    ),
                  ),
             ),





                Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
             decoration: new BoxDecoration(
                 
               ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: 


                Column(
  mainAxisSize: MainAxisSize.min,
  children: <Widget>[

 


    RaisedButton(
  color:  Color.fromRGBO(0, 63, 116, 1),
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    onPressed: (){
          Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => new curva(
                                        years1: yearsd,
                                        years2: yearsbubbled,
                                        oxy: ossigenov,
                                        )));
        },
  child: Text("Longévité de ma cuvée",style: TextStyle(color: Colors.white),textScaleFactor: 1.0),
)
   
   
   
  ],
)


             ),
           ),
         ),


         
              Padding(
               padding: const EdgeInsets.only(top: 12.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 75,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/logointro.png'),
                      ),
                    ),
                  ),
             )

             
         


    
        


      

          




       ],)
     
   
      
     
    
   
    
    );
  }
}

