import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import 'package:tappi/value/resultfr2.dart';
import 'package:shared_preferences/shared_preferences.dart';

class calcfrancese2 extends StatefulWidget {
  calcfrancese2({this.email});
  final String email;
  @override
  _calcfrancese2State createState() => new _calcfrancese2State();
}

class _calcfrancese2State extends State<calcfrancese2> {

    String testdato='';



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getlang().then(updatename);


    _dropDownMenuItems = getDropDownMenuItems();
    linear = _dropDownMenuItems[2].value;

    _dropDownMenuItemssugar = getDropDownMenuItemssugar();
    sugar = _dropDownMenuItemssugar[9].value;


     _dropDownMenuItemsformat = getDropDownMenuItemsformat();
    format = _dropDownMenuItemsformat[0].value;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
  }

    List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }


     List<DropdownMenuItem<String>> getDropDownMenuItemssugar() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _sugar) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

 List<DropdownMenuItem<String>> getDropDownMenuItemsformat() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _format) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

  int _zucchero = 15;
  
  var sizeofbottle="37,5cL";
  int _radioValue = 0;

  var diametro="29mm";
  int _radioValue2 = 0;


  var anni="1";

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
}
  
  void _sizeofbottle(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          sizeofbottle = "37,5cL";
          break;
        case 1:
          sizeofbottle = "75cL";
          break;
        case 2:
          sizeofbottle = "150cL";
          

          break;
      }
    });
  }

    
  void _diameterbottle(int value2) {
    setState(() {
      _radioValue2 = value2;
  
      switch (_radioValue2) {
        case 0:
          diametro = "29mm";
          break;
        case 1:
          diametro = "26mm";
          break;
       
      }
    });
  }

  List _cities =
  ["TOP", "TOP+3", "TOP+", "TOP Z","TOP S",];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String linear;

   void changedDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      linear = selectedCity;
    });
}

List _sugar =
  ["15", "16", "17", "18","19","20", "21", "22", "23", "24", "25", "26"];

  List<DropdownMenuItem<String>> _dropDownMenuItemssugar;
  String sugar;

   void changedDropDownItemsugar(String sugarselect) {
    print("Selected city $sugarselect, we are going to refresh the UI");
    setState(() {
      sugar = sugarselect;
    });
}

List _format =
  ["Standard (75cL)","Demi (37,5cL)", "Magnum (150cL)"];

  List<DropdownMenuItem<String>> _dropDownMenuItemsformat;
  String format="Standard (75cL)";

   void changedDropDownItemsformat(String formatselect) {
    print("Selected city $formatselect, we are going to refresh the UI");
    setState(() {
      format = formatselect;
    });
}

 Future<String> getlang() async{
     SharedPreferences prefs = await SharedPreferences.getInstance();
     String getdato=prefs.getString("dato");

     return getdato;
  }

  String riga1='Sucre au tirage (g/L)';
String riga2='Format de la bouteille:';
String riga3="Diamètre du goulot";
String riga4="Choix du joint de capsule";
String riga5="Temps sur lattes (en années)";
String riga6="Retourner";
String riga7="Appliquer la valeur";
String riga8="Définir"; 
String riga9="Résultats";
String riga10="Le format de bouteille de 150cL est compatible seulement avec le diamètre 29mm";
String riga11="Annuler";
String riga12="Continuer avec 29mm";
String riga20="Longévité de ma cuvée";
String banner="banner_fr.png";



void updatename(String value){
 print('lingua $value');
 if(value=='Français'){
    print("ciaoo sono francese");
    setState(() {
     this.testdato=value;
     this.riga1='Sucre au tirage (g/L)';
     this.riga2='Format de la bouteille:';
     this.riga3="Diamètre du goulot";
     this.riga4="Choix du joint de capsule";
     this.riga5="Temps sur lattes (en années)";
     this.riga6="Retourner";
     this.riga7="Appliquer la valeur";
     this.riga8="Définir";
     this.riga9="Résultats";
     this.riga10="Le format de bouteille de 150cL est compatible seulement avec le diamètre 29mm";
     this.riga11="Annuler";
     this.riga12="Continuer avec 29mm";
     this.riga20="Longévité de ma cuvée";
     this.banner="banner_fr.png";
     
    
  });

 }

 if(value=='English'){
    print("ciaoo sono inglese");
    setState(() {
     this.testdato=value;
     this.riga1='Added sugar at tirage (g/L)';
     this.riga2='Bottle size';
     this.riga3="Bottleneck diameter";
     this.riga4="Type of crown liner";
     this.riga5="Aging on lees in years";
     this.riga6="Return";
     this.riga7="Apply";
     this.riga8="Define";
     this.riga9="Results";
     this.riga10="The bottle with size 150cL is compatible only with the diameter 29mm";
     this.riga11="Cancel";
     this.riga12="Continue with 29mm";
     this.riga20="Effervescence longevity";
     this.banner="banner_en.png";
     
    
  });

 }

  if(value=='Italiano'){
    print("ciaoo sono italiano");
    setState(() {
     this.testdato=value;
     this.riga1='Zucchero aggiunto al tiraggio (g/L)';
     this.riga2='Formato della bottiglia:';
     this.riga3="Baga della bottiglia";
     this.riga4="Guarnizione del tappo";
     this.riga5="Tempo sui lieviti in anni";
     this.riga6="Annulla";
     this.riga7="Applica il valore";
     this.riga8="Imposta";
     this.riga9="Risultato";
     this.riga10="Il formato da 150cL è compatibile solo con il diametro da 29mm";
     this.riga11="Annulla";
     this.riga12="Continua con 29mm";
     this.riga20="Longevità dell'effervescenza";
     this.banner="banner_it.png";
  });

 }


  setState(() {
     this.testdato=value;
     
    
  });
  
  
  
}








  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomPadding:false,
 floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1),
        elevation: 2.0,
        icon: const Icon(Icons.navigate_next),
        label: Text("$riga9",textScaleFactor: 1.0),
        onPressed: () {
          if (format=="Magnum (150cL)"){ //controllo non esce da qua!!! 
             
             if (diametro=="26mm"){
              showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text(riga10,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'  )), 
            
        
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text(riga11,style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text(riga12,style: TextStyle(fontSize: 14)),
                onPressed:(){
                
                anni=anni.replaceAll(",", ".");
                Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => new resultfr2(
                    linear: linear,
                    diametro: "29mm",
                    sizeofbottle: format,
                    zucchero: sugar,
                    anni: anni,
                  )));
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );
        }
        else{

            
            
            anni=anni.replaceAll(",", ".");
            print('********************************************* $format');
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => new resultfr2(
                  linear: linear,
                  diametro: diametro,
                  sizeofbottle: format,
              // sizeofbottle: sizeofbottle,
                  zucchero: sugar,
                  anni: anni,
                )));
        }

          }
          else{
            

            anni=anni.replaceAll(",", ".");
            print(anni);
            

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => new resultfr2(
                  linear: linear,
                  diametro: diametro,
                  sizeofbottle: format,
                  zucchero: sugar,
                  anni: anni,
                )));

          }
          

          



        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('    ')
             // IconButton(
               // icon: Icon(Icons.menu),
             //   onPressed: () {},
             // ),
             // IconButton(
              //  icon: Icon(Icons.search),
              //  onPressed: () {},
             // )
            ],
          ),
        ),
),

    

     appBar: AppBar(
                backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    Text("$riga20",textScaleFactor: 1.0,style: new TextStyle(fontSize:16))
                    
                    
                    
                  ],
                ),
              ),
     body:
     
     
        
    
 

       Column(children: <Widget>[
           Padding(
             padding: const EdgeInsets.only(top:8.0),
             child: Container( 
              height: 140.0,
              width: double.infinity,
              decoration: BoxDecoration(

                image:DecorationImage( 
                 image: AssetImage(
                                "assets/image/language/banner.png"), 
                                alignment: Alignment.center
                ),
                color: Colors.white
             
              ),
              child: Column(
               mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                 // Text('Demo', style: TextStyle(fontSize: 40,color: Colors.white),textAlign: TextAlign.left,),
                ],
              ),
              
          
          
          ),
           ),

         
         
         
         //qua inizia il blocco inserimento

        
          

           Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(' $riga1  ',textScaleFactor: 1.0, style: TextStyle(fontSize: 18,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                   ),

                   

                                           MediaQuery(
          child: DropdownButton(
                    value: sugar,
                    items: 
                    _dropDownMenuItemssugar,
                    onChanged: changedDropDownItemsugar,
                   ),
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        ),

                   



       
        


                 ],),
               ),
             ),
           ),
         ),

           Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(' $riga2',textScaleFactor: 1.0, style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                   ),

                  

                                   MediaQuery(
          child: DropdownButton(
                    value: format,
                    items: 
                    _dropDownMenuItemsformat,
                    onChanged: changedDropDownItemsformat,
                   ),
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        ),



       
        


                 ],),
               ),
             ),
           ),
         ),

         
        

         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                     
                     Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Text(' $riga3 ',textScaleFactor: 1.0, style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                       ],
                     ),
                   
                     Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                      

                      new Radio( activeColor: Colors.black,
                                        value: 0,
                                        groupValue: _radioValue2,
                                        onChanged: _diameterbottle,
                                      ),
                                      new Text('29mm',textScaleFactor: 1.0, style: TextStyle(fontSize: 18,color: Colors.black),),
                                      new Radio(
                                         activeColor: Colors.black,
                                        value: 1,
                                        groupValue: _radioValue2,
                                        onChanged: _diameterbottle,
                                      ),
                                      new Text('26mm ',textScaleFactor: 1.0, style: TextStyle(fontSize: 18,color: Colors.black),),
                                      
                                     
        


                     ],),
                   ],
                 ),
               ),
             ),
           ),
         ),

         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(' $riga4  ',textScaleFactor: 1.0, style: TextStyle(fontSize: 18,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                   ),

                   

                        
                             MediaQuery(
          child: DropdownButton(
                    value: linear,
                    items: 
                    _dropDownMenuItems,
                    onChanged: changedDropDownItem,
                   ),
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        ),
           



       
        


                 ],),
               ),
             ),
           ),
         ),

          


          




       ],)
     
   
      
     
    
   
    
    );
  }
}

