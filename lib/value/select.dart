import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:tappi/value/francese.dart';

import 'package:tappi/value/francese2.dart';
import 'package:tappi/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
 

class select extends StatefulWidget {
  select({this.email});
  final String email;
  @override
  _selectState createState() => new _selectState();
}

class _selectState extends State<select> {
String salvataggio;
String testdato='';



  @override
  void initState() {
  
    print('salve');
    getlang().then(updatename);
    super.initState();
    


  }

String riga1='ÉCHANGES GAZEUX (O\u2082/CO\u2082) DE MA CUVÉE';
String riga2='LONGÉVITÉ DE MA CUVÉE';
String riga3="";


void updatename(String value){
 print('lingua $value');
 if(value=='Français'){
    print("ciaoo sono francese");
    setState(() {
     this.testdato=value;
     this.riga1='ÉCHANGES GAZEUX (O\u2082/CO\u2082) DE MA CUVÉE';
     this.riga2='LONGÉVITÉ DE MA CUVÉE';
     this.riga3="";
     
    
  });

 }

 if(value=='English'){
    print("ciaoo sono inglese");
    setState(() {
     this.testdato=value;
     this.riga1='PRESSURE CALCULATION';
     this.riga2='Effervescence';
     this.riga3="longevity";
     
    
  });

 }

  if(value=='Italiano'){
    print("ciaoo sono italiano");
    setState(() {
     this.testdato=value;
     this.riga1='Calcolo della pressione';
     this.riga2='Longevità';
     this.riga3="dell’effervescenza";   
  });

 }


  setState(() {
     this.testdato=value;
     
    
  });
  
  
  
}


Choice _selectedChoice = choices[0];


void _select(Choice choice) {
    // Causes the app to rebuild with the new _selectedChoice.
    
    print(choice.title); //estraggo la ligua 
    
    savelang(choice.title);
    reload();


  
  
  
  }

void reload(){
   Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => new SplashScreen(
                                         
                                        )));
}

 
  

  

  Future<bool> savelang(String dato) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    
    
    return prefs.setString("dato", dato);
    
  }

  Future<String> getlang() async{
     SharedPreferences prefs = await SharedPreferences.getInstance();
     String getdato=prefs.getString("dato");

     return getdato;
  }




  @override
  Widget build(BuildContext context) {

 
    
  return Scaffold(
    resizeToAvoidBottomPadding: false,
       appBar: AppBar(
                backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                    Text("Effervescence Predictor 2.0",textScaleFactor: 1.0,)
                    
                    
                  ],
                ),
                actions: <Widget>[

   //        PopupMenuButton<Choice>(
              //onSelected: _select,
     //         onSelected: _select,
       //       icon: Icon(Icons.language),            
       //       itemBuilder: (BuildContext context) {
      //          return choices.skip(0).map((Choice choice) {
       //           return PopupMenuItem<Choice>(
       //             value: choice,
       //             child: Text(choice.title),
       //           );
       //         }).toList();
        //      },
        //    ),
        ],


        ),
      
      body: Container(
              
              width: double.infinity,
              height: 900,
              decoration: BoxDecoration(
                color: Colors.white
              ),
              child: 
              Padding(
                padding: const EdgeInsets.only(top:40.0),
                child: Column(
                  children: <Widget>[
                     
             Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 Image.asset('assets/image/language/bolle.png', width: 93, height: 150,),
                 Padding(
                     padding: const EdgeInsets.only(top:20.0),
                     child: Material(
                          elevation: 4.0,
                          borderRadius: new BorderRadius.circular(10.0),
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Ink.image(
                                image: AssetImage('assets/image/language/rosso.png'),
                                fit: BoxFit.cover,
                                width: 150.0,
                                height: 150.0,
                                child: InkWell(
                                  onTap: () {
                                     Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => new calcfrancese(
                                         
                                        )));
                                  },
                                  child: null,
                                ),
                              ),
                              
                            ],
                            
                          ),

                        ),
                 ),
                 Image.asset('assets/image/language/bolle.png', width: 93, height: 150,),
               ],
             ),

             Padding(
               padding: const EdgeInsets.only(top:15.0),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                  Text(riga1.toUpperCase(),textScaleFactor: 1.0, style: new TextStyle(decoration: TextDecoration.none,fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(0, 63, 116, 1),fontFamily: 'RobotoMono'))
                 ],
               ),
             ),

            

               Padding(
                 padding: const EdgeInsets.only(top:40.0),
                 child: Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                   Image.asset('assets/image/language/bolle.png', width: 93, height: 150,),
                   Padding(
                       padding: const EdgeInsets.only(top:20.0),
                       child: Material(
                            elevation: 4.0,
                           
                            borderRadius: new BorderRadius.circular(10.0),
                            
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                Ink.image(
                                  image: AssetImage('assets/image/language/blu.png'),
                                  fit: BoxFit.cover,
                                  width: 150.0,
                                  height: 150.0,
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => new calcfrancese2(
                                         
                                        )));
                                    },
                                    child: null,
                                  ),
                                ),
                                
                              ],
                            ),

                          ),
                   ),
                   Image.asset('assets/image/language/bolle.png', width: 93, height: 150,),
                 ],
             ),
               ),
             
              Padding(
               padding: const EdgeInsets.only(top:15.0),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(riga2.toUpperCase(),textScaleFactor: 1.0, style: new TextStyle(decoration: TextDecoration.none,fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(0, 63, 116, 1),fontFamily: 'RobotoMono'),),
                      Text(riga3.toUpperCase(), textScaleFactor: 1.0, style: new TextStyle(decoration: TextDecoration.none,fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(0, 63, 116, 1),fontFamily: 'RobotoMono'),),

                    ],
                  )
                 ],
               ),
             ),

             

            


             



                  ],
                ),
              ),
            ),
  );
   
      
     
  
   
    
    
  }


 
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'English', icon: Icons.directions_car),
  const Choice(title: 'Français', icon: Icons.directions_bike),
  const Choice(title: 'Italiano', icon: Icons.directions_boat),

];

class SharedPreferencesHelper {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  static final String _kLanguageCode = "language";

  /// ------------------------------------------------------------
  /// Method that returns the user language code, 'en' if not set
  /// ------------------------------------------------------------
  static Future<String> getLanguageCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_kLanguageCode) ?? 'en';
  }

  /// ----------------------------------------------------------
  /// Method that saves the user language code
  /// ----------------------------------------------------------
  static Future<bool> setLanguageCode(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_kLanguageCode, value);
  }
}

